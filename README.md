Simple bots or Simbot's is a library designed to create simple chat bots using python. 

Its Basic functions :- 
 
1) Uses Machine learning to create Intents . 
2) Supports multiple ways of creating entities and relations . 
3) Also provides helper functions to query context stack .
4) Provides a framework to create text based chatbots .
5) Provides a framework to define test conversation of the bot and test it .
6) Support saving and loading existing conversation .
7) Support saving and loading trained Chatbot .
8) Supports Sub Conversation management and creation also supports subconversation trace.
9) Supports mongoDb integration to save bot logs.
10) Supports other database integration(relational) to save bot logs.

if you are unfamiliar with any of these terms check out the [documentation](https://infamousmlguy.gitlab.io/simple-bots/docs/) .

To install simply use 

    pip install simbots
    
Links
 
 1) [Documentation](https://infamousmlguy.gitlab.io/simple-bots/docs/) 
 2) [pypi](https://pypi.org/project/simbots/)
 3) [code](https://gitlab.com/infamousMLGuy/simple-bots)
 
Check out the [documentation](https://infamousmlguy.gitlab.io/simple-bots/docs/) page for chatbot terminology and how to solve some common problems like :


| Sr | Bot     | Documentation | Code Only | 
|  ----------- | ----------- | ----------- | ----------- | 
| 1) | How to make simple conversation .  | [step by step](https://infamousmlguy.gitlab.io/simple-bots/docs/#a-bot-for-simple-conversation)       |   [code only](https://gitlab.com/infamousMLGuy/simple-bots/-/blob/main/sampleBots/BotForSimpleConversation.ipynb)        |
| 2) | Convert site FAQs (Frequently asked questions) to a chatbot .   | [step by step](https://infamousmlguy.gitlab.io/simple-bots/docs/#how-to-turn-faq-s-into-bots)       |   [code only](https://gitlab.com/infamousMLGuy/simple-bots/-/blob/main/sampleBots/FaqsBot.ipynb)        |
| 3) | Detect and handle person names from chat texts .   | [step by step](https://infamousmlguy.gitlab.io/simple-bots/docs/#reply-using-name)       |   [code only](https://gitlab.com/infamousMLGuy/simple-bots/-/blob/main/sampleBots/ReplyUsingNameBot.ipynb)        |
| 4) | Create and Place restaurant orders from chat .  | [step by step](https://infamousmlguy.gitlab.io/simple-bots/docs/#take-order-bot-v1)       |   [code only](https://gitlab.com/infamousMLGuy/simple-bots/-/blob/main/sampleBots/TakeOrderBotV1.ipynb)        |
    
 
 