from flask import Flask,jsonify
from flask import request

from simbots.Bot import SubConversation
from simbots.Bot import Bot
from pymongo import MongoClient
import json



client = MongoClient('mongodb://localhost:27017/')
db = client['local']
collection= db["takeOrderBotV1Logs"]

import os
app = Flask(__name__)
@app.route('/getBotResponse', methods=['GET'])
def getBotResponse():

    message = request.args.get('message')
    conversationId = request.args.get("conversationId")
    currentDialogNumber = request.args.get("currentDialogNumber")
    currentDialogNumber = currentDialogNumber.replace('"', "")
    if currentDialogNumber =="":
        currentDialogNumber =None
    else:
        currentDialogNumber= int(currentDialogNumber)


    conversationId = conversationId.replace('"',"")

    bot = createBot()



    conversationId, outputMessage, currentDialogNumber = bot.run(theme="themeBasic", mode='singleMessageResponse',
                                                                  loadPath="takeOrderBotV1.p"
                                                                  , conversationId=conversationId,
                                                                  message=message,
                                                                  conversationSaveParams={
                                                                      "mongoConnector": collection
                                                                  }, saveTo="mongoDb",
                                                                  currentDialogNumber=currentDialogNumber,
                                                                  considerPreviousDialogs=3)

    

    returnMessage ={
        "message":outputMessage,
        "conversationId":conversationId,
        "currentDialogNumber":currentDialogNumber

    }
    return jsonify(returnMessage)


def enterTakeOrder(contextManager):
    # Will enter this loop if intent is order food

    currentIntentName = contextManager.findCurrentTopIntent()['name']

    return currentIntentName == "OrderFood"


def exitTakeOrder(contextManager):
    # Will exit loop if intent is Discard

    currentIntentName = contextManager.findCurrentTopIntent()['name']

    return currentIntentName == "Discard"


def checkIfPresentInSessionVariables(entityKind, sessionVariables):
    entityKindPresent = entityKind in sessionVariables.keys()

    entityValue = sessionVariables.get(entityKind, "")

    return entityKindPresent, entityValue


def enterLogicTakeOrder(contextManager, outputs=None):
    if outputs is None:
        outputs = []

    # Get Already Existing Entities

    if "sessionVariables" in contextManager.context.keys():
        sessionVariables = contextManager.context["sessionVariables"]
    else:
        sessionVariables = {}

    currentEntities = [entity for entity in contextManager.findCurrentEntities() if "Food" in entity["kind"]]

    # Update entites in context

    for entity in currentEntities:
        sessionVariables[entity["kind"]] = entity["value"]

    contextManager.context["sessionVariables"] = sessionVariables

    contextManager.updateContextTree()

    # Formulate reply

    foodTypePresent, foodType = checkIfPresentInSessionVariables("FoodType", sessionVariables)
    foodSizePresent, foodSize = checkIfPresentInSessionVariables("FoodSize", sessionVariables)
    foodNamePresent, foodName = checkIfPresentInSessionVariables("FoodName", sessionVariables)

    if foodNamePresent and foodTypePresent and foodSizePresent:
        stayInSubConversation = False

        contextManager.context["sessionVariables"] = {}
        contextManager.updateContextTree()
        foodType = foodType.replace("pizza", "").replace("burger", "")

        # Write function to save context variables here

        outputs.append({'tag': 'OrderFood.basic', 'data': [foodName, foodSize, foodType]})

    else:
        stayInSubConversation = True
        outputs.append({'tag': 'OrderFood.missing', 'data': [foodName, foodSize, foodType]})

    return stayInSubConversation, contextManager, outputs


def enterAllElse(contextManager):
    currentIntentName = contextManager.findCurrentTopIntent()['name']
    return currentIntentName != "OrderFood"


def enterLogicAllElse(contextManager, outputs=None):
    if outputs is None:
        outputs = []

    intentName = contextManager.findCurrentTopIntent()['name']

    reply = {'tag': '{0}.basic'.format(intentName), 'data': None}

    outputs.append(reply)

    stayInSubConversation = False

    return stayInSubConversation, contextManager, outputs


def createBot():


    subConversation1 = SubConversation(
        name="allElse",
        enter=enterAllElse,
        onEnterLogic=enterLogicAllElse,
    )

    subConversation2 = SubConversation(name="TakingOrder",
                                       enter=enterTakeOrder,
                                       exit=exitTakeOrder,
                                       onEnterLogic=enterLogicTakeOrder,
                                       )

    subConversations = [
        subConversation1,
        subConversation2,

    ]

    intentExamples = {

        'Irrelevant': ['the weather is fine today', 'the sun rises in the east',
                       'the quick brown fox jumps over the red carpet',
                       'the sun rises in the east',
                       'What is love , baby dont hurt me ',
                       'this is a new dawn a new day']
    }

    class NewBot(Bot):

        def reason(self):

            outputs = []
            # If user has asked any of the other questions except placing order execute this
            if self.subConversations[0].enterOn(self.contextManager):
                self.contextManager, outputs = self.subConversations[0].execute(self.contextManager, outputs)

            # If user was in the process of making an order execute this
            if self.subConversations[1].isHappening or self.subConversations[1].enterOn(self.contextManager):
                self.contextManager, outputs = self.subConversations[1].execute(self.contextManager, outputs)

            return outputs

    newB = NewBot(intentExamples, {}, {},
                  confidenceLimit=0, subConversations=subConversations)

    return newB



if __name__ =='__main__':
    app.run()