from flask import Flask,jsonify
from flask import request

from simbots.Bot import SubConversation
from simbots.Bot import Bot
import sqlite3

import json



conn = None


def get_connection():
    global conn
    if not conn:
        conn = sqlite3.connect("takeOrderBotV1Logs.db",check_same_thread=False)
    return conn


## functions for reading

def dialogsFromTable(conversationId, lowerLimit):
    conn = get_connection()
    rows = list(conn.execute(
        "Select * from dialogs where dialogNumber > {0} and conversationId ='{1}'".format(lowerLimit, conversationId)))

    rows = sorted(rows, key=lambda x: (x[0]))
    dialogs = []
    for row in rows:
        dialogs.append(row[2])
        conversationId = row[1]
    return conversationId, dialogs


def sessionVariablesFromTable(conversationId, lowerLimit):
    conn = get_connection()
    rows = list(conn.execute(
        "Select * from sessionvariables where dialogNumber > {0} and conversationId ='{1}'".format(lowerLimit,
                                                                                                   conversationId)))

    rows = sorted(rows, key=lambda x: (x[0]))
    sessionVariables = {}
    for row in rows:
        sessionVariables[row[2]] = row[3]
        conversationId = row[1]
    return conversationId, sessionVariables


def subConversationStatusFromTable(conversationId, lowerLimit):
    conn = get_connection()
    rows = list(conn.execute(
        "Select * from subconversationstatus where dialogNumber > {0} and conversationId ='{1}'".format(lowerLimit,
                                                                                                        conversationId)))

    rows = sorted(rows, key=lambda x: (x[0]))
    subConversationStatus = {}
    for row in rows:
        subConversationStatus[row[2]] = bool(row[3])
        conversationId = row[1]
    return conversationId, subConversationStatus


def subConversationPathsFromTable(conversationId, lowerLimit):
    conn = get_connection()
    rows = list(conn.execute(
        "Select * from subconversationpaths where dialogNumber > {0} and conversationId ='{1}'".format(lowerLimit,
                                                                                                       conversationId)))

    rows = sorted(rows, key=lambda x: (x[0]))
    subConversationPaths = {}
    for row in rows:
        if str(row[7]) not in subConversationPaths.keys():
            subConversationPaths[str(row[7])] = []

        subConversationPaths[str(row[7])].append(
            {
                "name": row[3],
                "subConversationId": row[4],
                "method": row[5],
                "isHappening": bool(row[6])

            }

        )
        conversationId = row[1]

    return conversationId, subConversationPaths


def datetimeFormatter2(dateAsString):
    # sendtime = "16/12/2021, 23:49:11"
    return dt.strptime(dateAsString, "%Y-%m-%d %H:%M:%S").strftime("%d/%m/%Y, %H:%M:%S")


def messagesFromTable(conversationId, lowerLimit):
    conn = get_connection()
    rows = list(conn.execute(
        "Select * from messages where dialogNumber > {0} and conversationId ='{1}'".format(lowerLimit, conversationId)))
    rows = sorted(rows, key=lambda x: (x[0]))
    messages = []
    for row in rows:
        messages.append(
            {
                "text": row[2],
                "entitiesSub": row[3],
                "messageType": row[4],
                "dialogNumber": row[5],
                "sendTime": datetimeFormatter2(row[6])

            }
        )
        conversationId = row[1]

    return conversationId, messages


def entitiesFromTable(conversationId, lowerLimit):
    conn = get_connection()
    rows = list(conn.execute(
        "Select * from entities where dialogNumber > {0} and conversationId ='{1}'".format(lowerLimit, conversationId)))
    rows = sorted(rows, key=lambda x: (x[0]))
    entities = []
    for row in rows:
        entities.append(
            {
                "value": row[2],
                "exactValue": row[3],
                "kind": row[4],
                "dialogNumber": row[5],
                "foundAt": [row[6], row[7]]

            }

        )
        conversationId = row[1]
    return conversationId, entities


def intentsFromTable(conversationId, lowerLimit):
    conn = get_connection()
    rows = list(conn.execute(
        "Select * from entities where dialogNumber > {0} and conversationId ='{1}'".format(lowerLimit, conversationId)))
    rows = sorted(rows, key=lambda x: (x[0]))
    intents = []
    for row in rows:
        intents.append(
            {
                "name": row[2],
                "confidence": row[3],
                "dialogNumber": row[4],
                "rank": row[5]

            }
        )
        conversationId = row[1]
    return conversationId, intents


## functions for writing

from datetime import datetime as dt


def datetimeFormatter(dateAsString):
    # sendtime = "16/12/2021, 23:49:11"
    return dt.strptime(dateAsString, "%d/%m/%Y, %H:%M:%S").strftime("%Y-%m-%d %H:%M:%S")


def intentsToTable(conversationId, intents, dialogNumber):
    intentsAsTable = []
    for intent in intents:
        intentsAsTable.append(
            (conversationId, intent["name"], intent["confidence"], intent["dialogNumber"], intent["rank"]))

    conn = get_connection()
    conn.executemany("INSERT INTO intents VALUES (null,?, ?,?,?,?)", intentsAsTable)
    conn.commit()


def entitiesToTable(conversationId, entities, dialogNumber):
    entitiesAsTable = []
    for entity in entities:
        entitiesAsTable.append(
            (conversationId, entity["value"], entity["exactValue"], entity["kind"], entity["dialogNumber"],
             entity["foundAt"][0], entity["foundAt"][1]
             ))
    conn = get_connection()
    conn.executemany("INSERT INTO entities VALUES (null,?, ?,?,?,?,?,?)", entitiesAsTable)
    conn.commit()


def messagesToTable(conversationId, messages, dialogNumber):
    messagesAsTable = []

    for message in messages:
        messagesAsTable.append((conversationId,
                                message["text"],
                                message["entitiesSub"],
                                message["messageType"],
                                message["dialogNumber"],
                                datetimeFormatter(message["sendTime"])
                                ))
    conn = get_connection()
    conn.executemany("INSERT INTO messages VALUES (null,?, ?,?,?,?,?)", messagesAsTable)
    conn.commit()


def subConversationPathsToTable(conversationId, subconversationPaths, dialogNumber):
    subconversationPathsAsTable = []

    for key in subconversationPaths.keys():
        dialogNumber = int(key)
        for sr, sub in enumerate(subconversationPaths[key]):
            row = (conversationId, sr, sub["name"], sub["subConversationId"], sub["method"], sub["isHappening"], key)
            subconversationPathsAsTable.append(row)

    conn = get_connection()
    conn.executemany("INSERT INTO subconversationpaths VALUES (null,?, ?,?,?,?,?,?)", subconversationPathsAsTable)
    conn.commit()


def subConversationStatusToTable(conversationId, subconversationStatus, dialogNumber):
    subconversationStatusAsTable = []
    for subConversationId in subconversationStatus.keys():
        row = (conversationId, subConversationId, subconversationStatus[subConversationId], dialogNumber)
        subconversationStatusAsTable.append(row)

    conn = get_connection()
    conn.executemany("INSERT INTO subconversationstatus VALUES (null,?,?,?,?)", subconversationStatusAsTable)
    conn.commit()


def sessionVariablesToTable(conversationId, sessionVariables, dialogNumber):
    sessionVariablesAsTable = []
    for variableName in sessionVariables.keys():
        sessionVariablesAsTable.append((conversationId, variableName, sessionVariables[variableName], dialogNumber))

    conn = get_connection()
    conn.executemany("INSERT INTO sessionvariables VALUES (null,?,?,?,?)", sessionVariablesAsTable)
    conn.commit()


def dialogsToTable(conversationId, dialogs, dialogNumber):
    conn = get_connection()
    dialogs = [(conversationId, dialog) for dialog in dialogs]
    conn.executemany("INSERT INTO dialogs VALUES (null,?,?)", dialogs)
    conn.commit()

def enterTakeOrder(contextManager):
    # Will enter this loop if intent is order food

    currentIntentName = contextManager.findCurrentTopIntent()['name']

    return currentIntentName == "OrderFood"


def exitTakeOrder(contextManager):
    # Will exit loop if intent is Discard

    currentIntentName = contextManager.findCurrentTopIntent()['name']

    return currentIntentName == "Discard"


def checkIfPresentInSessionVariables(entityKind, sessionVariables):
    entityKindPresent = entityKind in sessionVariables.keys()

    entityValue = sessionVariables.get(entityKind, "")

    return entityKindPresent, entityValue


def enterLogicTakeOrder(contextManager, outputs=None):
    if outputs is None:
        outputs = []

    # Get Already Existing Entities

    if "sessionVariables" in contextManager.context.keys():
        sessionVariables = contextManager.context["sessionVariables"]
    else:
        sessionVariables = {}

    currentEntities = [entity for entity in contextManager.findCurrentEntities() if "Food" in entity["kind"]]

    # Update entites in context

    for entity in currentEntities:
        sessionVariables[entity["kind"]] = entity["value"]

    contextManager.context["sessionVariables"] = sessionVariables

    contextManager.updateContextTree()

    # Formulate reply

    foodTypePresent, foodType = checkIfPresentInSessionVariables("FoodType", sessionVariables)
    foodSizePresent, foodSize = checkIfPresentInSessionVariables("FoodSize", sessionVariables)
    foodNamePresent, foodName = checkIfPresentInSessionVariables("FoodName", sessionVariables)

    if foodNamePresent and foodTypePresent and foodSizePresent:
        stayInSubConversation = False

        contextManager.context["sessionVariables"] = {}
        contextManager.updateContextTree()
        foodType = foodType.replace("pizza", "").replace("burger", "")

        # Write function to save context variables here

        outputs.append({'tag': 'OrderFood.basic', 'data': [foodName, foodSize, foodType]})

    else:
        stayInSubConversation = True
        outputs.append({'tag': 'OrderFood.missing', 'data': [foodName, foodSize, foodType]})

    return stayInSubConversation, contextManager, outputs


def enterAllElse(contextManager):
    currentIntentName = contextManager.findCurrentTopIntent()['name']
    return currentIntentName != "OrderFood"


def enterLogicAllElse(contextManager, outputs=None):
    if outputs is None:
        outputs = []

    intentName = contextManager.findCurrentTopIntent()['name']

    reply = {'tag': '{0}.basic'.format(intentName), 'data': None}

    outputs.append(reply)

    stayInSubConversation = False

    return stayInSubConversation, contextManager, outputs


def createBot():


    subConversation1 = SubConversation(
        name="allElse",
        enter=enterAllElse,
        onEnterLogic=enterLogicAllElse,
    )

    subConversation2 = SubConversation(name="TakingOrder",
                                       enter=enterTakeOrder,
                                       exit=exitTakeOrder,
                                       onEnterLogic=enterLogicTakeOrder,
                                       )

    subConversations = [
        subConversation1,
        subConversation2,

    ]

    intentExamples = {

        'Irrelevant': ['the weather is fine today', 'the sun rises in the east',
                       'the quick brown fox jumps over the red carpet',
                       'the sun rises in the east',
                       'What is love , baby dont hurt me ',
                       'this is a new dawn a new day']
    }

    class NewBot(Bot):

        def reason(self):

            outputs = []
            # If user has asked any of the other questions except placing order execute this
            if self.subConversations[0].enterOn(self.contextManager):
                self.contextManager, outputs = self.subConversations[0].execute(self.contextManager, outputs)

            # If user was in the process of making an order execute this
            if self.subConversations[1].isHappening or self.subConversations[1].enterOn(self.contextManager):
                self.contextManager, outputs = self.subConversations[1].execute(self.contextManager, outputs)

            return outputs

    newB = NewBot(intentExamples, {}, {},
                  confidenceLimit=0, subConversations=subConversations)

    return newB


app = Flask(__name__)


@app.route('/getBotResponse', methods=['GET'])
def getBotResponse():
    message = request.args.get('message')
    conversationId = request.args.get("conversationId")
    currentDialogNumber = request.args.get("currentDialogNumber")
    currentDialogNumber = currentDialogNumber.replace('"', "")
    if currentDialogNumber == "":
        currentDialogNumber = None
    else:
        currentDialogNumber = int(currentDialogNumber)

    conversationId = conversationId.replace('"', "")

    bot = createBot()

    conversationSaveParams = {"contextParameters": {
        "intents": {
            "read": intentsFromTable,
            "write": intentsToTable

        },
        "entities": {
            "read": entitiesFromTable,
            "write": entitiesToTable

        },
        "messages": {
            "read": messagesFromTable,
            "write": messagesToTable

        },
        "sessionVariables": {
            "read": sessionVariablesFromTable,
            "write": sessionVariablesToTable
        },
        "subConversationStatus": {
            "read": subConversationStatusFromTable,
            "write": subConversationStatusToTable

        },
        "subConversationPath": {
            "read": subConversationPathsFromTable,
            "write": subConversationPathsToTable
        },
        "dialogs": {
            "read": dialogsFromTable,
            "write": dialogsToTable
        }

    }}

    try:
       conversationId, outputMessage, currentDialogNumber = bot.run(theme="themeBasic", mode='singleMessageResponse',
                                                                  loadPath="takeOrderBotV1.p"
                                                                  , conversationId=conversationId,
                                                                  message=message,
                                                                  conversationSaveParams=conversationSaveParams,
                                                                  saveTo="custom",
                                                                  currentDialogNumber=currentDialogNumber,
                                                                  considerPreviousDialogs=3)
    except:
        print(json.dumps(bot.contextManager.context,indent=2))

    returnMessage = {
        "message": outputMessage,
        "conversationId": conversationId,
        "currentDialogNumber": currentDialogNumber

    }
    return jsonify(returnMessage)



if __name__ =='__main__':
    app.run()
    conn = get_connection()
    conn.close()

