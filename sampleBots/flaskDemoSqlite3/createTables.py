import sqlite3

conn = sqlite3.connect("takeOrderBotV1Logs.db")

print("Opened database successfully")

conn.execute("""DROP TABLE IF EXISTS INTENTS;""")


conn.execute('''CREATE TABLE intents
         (ID INTEGER  PRIMARY KEY AUTOINCREMENT ,
         CONVERSATIONID KEY      NOT NULL,
         NAME           TEXT     NOT NULL,
         CONFIDENCE     REAL     NOT NULL,
         DIALOGNUMBER   INT,
         RANK         INT);''')

print("Table created successfully")

conn.execute("""DROP TABLE IF EXISTS entities;""")

conn.execute('''CREATE TABLE entities
         (ID INTEGER  PRIMARY KEY AUTOINCREMENT ,
         CONVERSATIONID KEY      NOT NULL,
         value           TEXT     NOT NULL,
         exactvalue      TEXT     NOT NULL,
         kind      TEXT     NOT NULL,
         DIALOGNUMBER   INT,
         foundat_0         INT,
         foundat_1         INT);''')

print("Table created successfully")


conn.execute("""DROP TABLE IF EXISTS messages;""")

conn.execute('''CREATE TABLE messages
         (ID INTEGER    PRIMARY KEY AUTOINCREMENT ,
         CONVERSATIONID KEY      NOT NULL,
         text           TEXT     NOT NULL,
         entitiesSub    TEXT     NOT NULL,
         messageType    TEXT     NOT NULL,
         DIALOGNUMBER   INT,
         sendTime   TEXT NOT NULL);''')

conn.execute("""DROP TABLE IF EXISTS subconversationpaths;""")
conn.execute("""DROP TABLE IF EXISTS subconversations;""")

conn.execute('''
         CREATE TABLE subconversationpaths
         (ID INTEGER    PRIMARY KEY AUTOINCREMENT ,
         CONVERSATIONID KEY      NOT NULL,
         srno         Int  NOT NULL,
         name           TEXT     NOT NULL,
         subConversationId    KEY     NOT NULL,
         method    TEXT     NOT NULL,
         isHappening   INT,
         dialogNumber int);


'''

)
print("Table created successfully")

conn.execute("""DROP TABLE IF EXISTS subconversationstatus;""")

conn.execute('''
         CREATE TABLE subconversationstatus
         (ID INTEGER   PRIMARY KEY AUTOINCREMENT ,
          CONVERSATIONID KEY      NOT NULL,
          subconversationid key NOT NULL,
          isHappening    Int  NOT NULL,
          dialogNumber int);


'''

)

print("Table created successfully")

conn.execute("""DROP TABLE IF EXISTS sessionvariables;""")

conn.execute('''
         CREATE TABLE sessionvariables
         (ID INTEGER   PRIMARY KEY AUTOINCREMENT ,
         CONVERSATIONID KEY      NOT NULL,
         variableName    Text  NOT NULL,
         variableValue   Text NOT NULL,
         dialogNumber int);


''')

print("Table created successfully")
conn.execute("""DROP TABLE IF EXISTS dialogs;""")
conn.execute('''
         CREATE TABLE dialogs
         (ID INTEGER   PRIMARY KEY AUTOINCREMENT ,
         CONVERSATIONID KEY      NOT NULL,
         dialogNumber int);


'''

)

print("Table created successfully")
conn.commit()
conn.close()